package com.hendisantika.rqueuetaskexecutor.controller;

import com.github.sonus21.rqueue.producer.RqueueMessageSender;
import com.hendisantika.rqueuetaskexecutor.domain.Email;
import com.hendisantika.rqueuetaskexecutor.domain.Invoice;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : rqueue-task-executor
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/01/20
 * Time: 07.58
 */
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class AsyncController {
    private @NonNull
    RqueueMessageSender rqueueMessageSender;

    @Value("${email.queue.name}")
    private String emailQueueName;

    @Value("${invoice.queue.name}")
    private String invoiceQueueName;

    @Value("${invoice.queue.delay}")
    private Long invoiceDelay;

    @GetMapping("email")
    public String sendEmail(
            @RequestParam String email, @RequestParam String subject, @RequestParam String content) {
        log.info("Sending email");
        rqueueMessageSender.put(emailQueueName, new Email(email, subject, content));
        return "Please check your inbox!";
    }

    @GetMapping("invoice")
    public String generateInvoice(@RequestParam String id, @RequestParam String type) {
        log.info("Generate invoice");
        rqueueMessageSender.put(invoiceQueueName, new Invoice(id, type), invoiceDelay);
        return "Invoice would be generated in " + invoiceDelay + " milliseconds";
    }
}
