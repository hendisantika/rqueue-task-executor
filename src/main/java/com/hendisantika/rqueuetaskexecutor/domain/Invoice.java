package com.hendisantika.rqueuetaskexecutor.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : rqueue-task-executor
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/01/20
 * Time: 07.55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private String id;
    private String type;
}