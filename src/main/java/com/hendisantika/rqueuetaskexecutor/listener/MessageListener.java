package com.hendisantika.rqueuetaskexecutor.listener;

import com.github.sonus21.rqueue.annotation.RqueueListener;
import com.hendisantika.rqueuetaskexecutor.domain.Email;
import com.hendisantika.rqueuetaskexecutor.domain.Invoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : rqueue-task-executor
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/01/20
 * Time: 07.56
 */
@Component
@Slf4j
public class MessageListener {

    @RqueueListener(value = "${email.queue.name}")
    public void sendEmail(Email email) {
        log.info("Email {}", email);
    }

    @RqueueListener(delayedQueue = "true", value = "${invoice.queue.name}")
    public void generateInvoice(Invoice invoice) {
        log.info("Invoice {}", invoice);
    }
}