# Asynchronous Task Execution Using Redis and Spring Boot

## Spring/Spring Boot

Spring is the most popular framework available for Java application development. As such, Spring has one of the largest open-source communities. Besides that, Spring provides extensive and up-to-date documentation that covers the inner workings of the framework and sample projects on their blog — there are 100K+ questions on StackOverflow.

In the early days, Spring only supported XML-based configuration, and because of that, it was prone to many criticisms. Later, Spring introduced an annotation-based configuration that changed everything. Spring 3.0 was the first version that supported the annotation-based configuration. In 2014, Spring Boot 1.0 was released, completely changing how we look at the Spring framework ecosystem. A more detailed timeline can be found here.

## Redis

Redis is one of the most popular NoSQL in-memory databases. Redis supports different types of data structures.  Redis supports different types of data structures e.g. Set, Hash table, List, simple key-value pair just name a few. The latency of Redis call is sub-milliseconds, support of a replica set, etc. The latency of the Redis operation is sub-milliseconds that makes it even more attractive across the developer community.

## Why Asynchronous task execution

A typical API call consists of five things

    1. Execute one or more database(RDBMS/NoSQL) queries
    2. One or more operations on some cache systems (In-Memory, Distributed, etc )
    3. Some computations (it could be some data crunching doing some math operations)
    4. Calling some other service(s) (internal/external)
    5. Schedule one or more tasks to be executed at a later time or immediately but in the background.

A task can be scheduled at a later time for many reasons. For example, an invoice must be generated 7 days after the order creation or shipment. Similarly, email notifications need not be sent immediately, so we can make them delayed.

With these real-world examples in mind, sometimes, we need to execute tasks asynchronously to reduce API response time. For example, if we delete 1K+ records at once, and if we delete all of these records in the same API call, then the API response time would be increased for sure. To reduce API response time, we can run a task in the background that would delete those records.

## Rqueue

Rqueue is a message broker built for the spring framework that stores data in Redis and provides a mechanism to execute a task at any arbitrary delay. Rqueue is backed by Redis since Redis has some advantages over the widely used queuing systems like Kafka, SQS. In most of the web applications backend, Redis is used to store either cache data or for some other purpose. In today's world, 8.4% web applications are using the Redis database.

Generally, for a queue, we use either Kafka/SQS or some other systems these systems bring an additional overhead in different dimensions e.g money which can be reduced to zero using Rqueue and Redis.

Apart from the cost if we use Kafka then we need to do infrastructure setup, maintenance i.e. more ops, as most of the applications are already using Redis so we won’t have ops overhead, in fact, same Redis server/cluster can be used with Rqueue. Rqueue supports an arbitrary delay.

GET Request: http://localhost:8080/email?email=xample@exampl.com&subject=%22test%20email%22&content=%22testing%20email%22

Below is invoice scheduling after 30 seconds:

http://localhost:8080/invoice?id=INV-1234&type=PROFORMA